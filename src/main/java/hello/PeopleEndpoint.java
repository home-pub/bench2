package hello;

import java.net.URI;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import hello.entities.Person;

@Path("/people")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Transactional
public class PeopleEndpoint {

    @Inject
    EntityManager em;

    @GET
    public List<Person> findAll() {
        return em.createQuery("SELECT p FROM Person p", Person.class)
                .getResultList();
    }

    @GET
    @Path("/{id}")
    public Person get(@PathParam("id") long id) {
        return em.find(Person.class, id);
    }

    @POST
    public Response save(Person p) {
        em.persist(p);
        return Response.created(URI.create("/people/" + p.getId()))
                .build();
    }
}
